<?php
// $Id: publication_date.module,v 1.1 2009/07/20 19:49:00 cleverage Exp $
/**
 * @file
 * Add a field containing the publication date.
 *
 * @author Clever Age
 * @author Emmanuelle Gouleau
 * @author Tristan Marly
 *
 * Initial D7 Port: Joost van der Locht
 */


/**
 * Implements hook_node_load().
 */
function publication_date_node_load($nodes, $types) {
  foreach ($nodes as $node) {
    $node->published_at = _publication_date_get_date($node->nid);

    // we have to manage the 'old nodes', i.e nodes that have been published BEFORE the activation
    // of this module.
    if (!$node->published_at) {
      $row = db_select('node', 'n')
        ->fields('n', array('created', 'status'))
        ->condition('nid', $node->nid)
        ->execute()
        ->fetchAssoc();
      if ($row && $row['status'] == 1) {
        $node->published_at = $row['created'];
      }
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function publication_date_node_insert($node) {
  // save publication date
  _publication_date_set_date($node, 'insert');
}

/**
 * Implements hook_node_update().
 */
function publication_date_node_update($node) {
  // save publication date
  _publication_date_set_date($node, 'update');
}

/**
 * Implements hook_node_delete().
 */
function publication_date_node_delete($node) {

  // Call a db_delete anyway
  db_delete('publication_date')
    ->condition('nid', $node->nid)
    ->execute();

}

/**
 * Worker function to save the published date to the database.
 *
 * @param object $node
 *   The node object.
 * @param string $op
 *   The node opperation being performed:
 *   - 'insert': a new node was created
 *   - 'update': an existing node was updated
 *
 * @see hook_node_insert()
 * @see hook_node_update()
 */
function _publication_date_set_date($node, $op = '') {
  // Set a default publication date value.
  $published_at = empty($node->published_at) ? 0 : $node->published_at;

  // If no publication date has been set and the node is published then use
  // REQUEST_TIME. Otherwise, use the default publication date.
  $published_at = ($published_at == 0 && $node->status == 1) ? REQUEST_TIME : $published_at;

  // Allow other modules to alter the publication date before it is saved.
  drupal_alter('publication_date', $published_at, $node, $op);

  // Update the node object.
  $node->published_at = $published_at;

  // Save the publication date to the database.
  db_merge('publication_date')
    ->key(array('nid' => $node->nid))
    ->fields(array('published_at' => $published_at))
    ->execute();
}

/**
 * @return the publication date for the given node, or false if the node is not published
 */
function _publication_date_get_date($nid) {
  $date = db_query("SELECT published_at FROM {publication_date} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
  return $date;
}

/**
 * Implements hook_views_api().
 */
function publication_date_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'publication_date') . '/includes',
  );
}

/**
 * Implementation of hook_form_BASE_ID_alter().
 * Display the publication date on the node edit form
 * @note: This wont work where you have displaysuite/rel enabled
 */
function publication_date_form_node_form_alter(&$form, &$form_state, $form_id) {
  $node = $form["#node"];
  $form['author']['pubdate'] = array(
    '#type' => 'textfield',
    '#title' => t('Published on'),
    '#maxlength' => 25,
    '#description' => t('Format: %time. Leave blank to use the time of form submission.', array('%time' => format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s O'))),
  );
  if ($form['nid'] !== NULL && isset($node->published_at) && $node->published_at) {
    $form['author']['pubdate']['#default_value'] = format_date($node->published_at, 'custom', 'Y-m-d H:i:s O');
  }
  $form['#validate'][] = 'publication_date_pubdate_validate';
  $form['#submit'][] = 'publication_date_pubdate_submit';
}

/**
 * Validate the published date input
 */
function publication_date_pubdate_validate($form, &$form_state) {
  // Validate the "authored on" field. As of PHP 5.1.0, strtotime returns FALSE instead of -1 upon failure.
  if (!empty($form_state['values']['pubdate'])) {
    if (strtotime($form_state['values']['pubdate']) <= 0) {
      form_set_error('pubdate', t('You have to specify a valid date for the published on field.'));
    }
  }
}

/**
 * Update the published date to Epoch integer for other hook implementations to deal with
 */
function publication_date_pubdate_submit($form, &$form_state) {
  // Set $node->published_at to the publication date field value, if it was set,
  // or zero if it was not.
  $form_state['node']->published_at = empty($form_state['values']['pubdate']) ? 0 : strtotime(($form_state['values']['pubdate']));
}

/**
 * Implements hook_entity_property_info().
 */
function publication_date_entity_property_info() {
  $info = array();
  $properties = &$info['node']['properties'];

  $properties['published_at'] = array(
    'label' => t('Published at'),
    'type' => 'date',
    'description' => t('Date of node publication.'),
    'setter permission' => 'administer nodes',
  );

  return $info;
}
